defmodule Roma do
  def calculate(input) when is_integer(input) do
    Roma.RomanNumeralConverter.convert(input)
  end

  def calculate(input) when is_binary(input) do
    Roma.NumberConverter.convert(input)
  end
end
