defmodule Roma.RomanNumeralConverter do
  @symbol_values [I: 1, IV: 4, V: 5, IX: 9, X: 10, XL: 40, L: 50, XC: 90, C: 100, CD: 400, D: 500, CM: 900, M: 1000]

  @moduledoc """
  Documentation for `Roma`.
  """

  def convert(input) when not is_integer(input) do
    raise ArgumentError, "Not a number: #{input}"
  end

  def convert(input) when input < 0 do
    raise ArgumentError, "Number must be positive."
  end

  def convert(input) when input == 0 do
    :nulla
  end

  @doc """
  Calculate Roman Numeral from an number of (Arabic) integers.

  ## Examples

      iex> Roma.convert(11)
      "XI"

      iex> Roma.convert(57)
      "LVII"

      iex> Roma.convert(1977)
      "MCMLXXVII"

  """
  def convert(int) do
    ordered_symbol_list = @symbol_values
                          |> Enum.sort(&((&1 |> elem(1)) < (&2 |> elem(1))))
                          |> Enum.reverse

    calculate_next(int, ordered_symbol_list, [])
    |> Enum.filter(fn e -> e |> elem(1) > 0 end)
    |> Enum.reduce("", fn e, acc -> "#{acc}#{print_symbols(e)}" end)
  end

  defp calculate_next(int, [next_symbol | tail], acc) do
    value = next_symbol |> elem(1)
    count = count(int, value)
    acc ++ [{(next_symbol |> elem(0)), count}] ++ calculate_next((int - (count * value)), tail, acc)
  end

  defp calculate_next(_int, [], acc) do
    acc
  end

  defp count(int, value) do
    floor(int / value)
  end

  defp print_symbols(count_tuple) do
    Range.new(1, count_tuple |> elem(1))
    |> Enum.reduce("", fn _, acc -> "#{acc}#{count_tuple |> elem(0)}" end)
  end
end
