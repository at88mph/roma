defmodule Roma.NumberConverter do
  @symbol_pattern ~r/(M*)?(CM)?(D)?(CD)?(C{0,3})(XC)?(L{0,1})(XL)?(X{0,3})(IX)?(V)?(IV)?(I{0,3})/iu
  @symbol_values [I: 1, IV: 4, V: 5, IX: 9, X: 10, XL: 40, L: 50, XC: 90, C: 100, CD: 400, D: 500, CM: 900, M: 1000]

  @moduledoc """
  Accept a String (binary) and convert it to a number of (Arabic) integers.
  """

  def convert(input) when not is_binary(input) do
    raise ArgumentError, "Not a valid string input: #{input}"
  end

  @doc """
  Calculate Roman Numeral from an number of (Arabic) integers.

  ## Examples

      iex> Roma.convert("MCMLXXVII")
      1977

      iex> Roma.convert("M")
      1000

      iex> Roma.convert("MMXXIII")
      2023
  """
  def convert(input) do
    [_head | tail] = Regex.run(@symbol_pattern, input)
    tail
    |> Enum.with_index
    |> Enum.filter(fn {symbol, _index} -> symbol != "" end)
    |> reduce
  end

  defp reduce([]) do
    raise ArgumentError, "Not a valid Roman Numeral."
  end

  defp reduce(matches) do
    matches
    |> Enum.reduce(0, fn {symbol_match, index}, acc -> calculate(symbol_match, index, acc) end)
  end

  defp calculate(symbol_match, index, acc) do
    {symbol, value} = @symbol_values
                      |> Enum.sort(&((&1 |> elem(1)) < (&2 |> elem(1))))
                      |> Enum.reverse
                      |> Enum.at(index)
    count = symbol_match |> String.split(Atom.to_string(symbol)) |> Enum.drop(1) |> Enum.count
    acc + (value * count)
  end
end
