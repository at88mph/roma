defmodule RomaTest do
  use ExUnit.Case
  doctest Roma

  test "Convert 11" do
    assert Roma.calculate(11) == "XI"
  end

  test "Convert 1977" do
    assert Roma.calculate(1977) == "MCMLXXVII"
  end

  test "Convert 2023" do
    assert Roma.calculate(2023) == "MMXXIII"
  end

  test "Error negative" do
    assert_raise ArgumentError, fn -> Roma.calculate(-11) end
  end

  test "Error not a number" do
    assert_raise ArgumentError, fn -> Roma.calculate("bogus") end
  end

  test "Convert MMXXIII" do
    assert Roma.calculate("MMXXIII") == 2023
  end

  test "Convert MCMLXXVII" do
    assert Roma.calculate("MCMLXXVII") == 1977
  end

  test "Convert MM" do
    assert Roma.calculate("MM") == 2000
  end

  test "Convert I" do
    assert Roma.calculate("I") == 1
  end

  test "Bogus input" do
    assert_raise ArgumentError, fn -> Roma.calculate("BOGUS") end
  end

  test "Both directions" do
    assert Roma.calculate(Roma.calculate("MMXVII")) == "MMXVII"
  end
end
