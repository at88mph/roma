# Roma

Library to convert a number of (Arabic) integers into Roman numerals.

## Example

```elixir
iex> Roma.calculate(1977)
"MCMLXXVII"
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `roma` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:roma, "~> 0.1.0"}
  ]
end
```
